package View;


import Controller.Riel_to_Other_Controller;
import Model.ArithmeticModel;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Riel_to_other extends JFrame {
    private JPanel jPanel;
    private JPanel jPaneloutput;

    private JLabel jLabelF;


    private JTextField Riel;


    private JButton submit;
    private JButton back;

    private JLabel USD;
    private JLabel Euro;
    private JLabel Frank;
    private JLabel Pound;
    private JLabel Baht;


    private int top = 20, left = 20, bottom = 20, right = 20;
    private Insets i = new Insets(top, left, bottom, right);

    private void input() {
        jPanel = new JPanel();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = GridBagConstraints.WEST;

        jLabelF = new JLabel("Input in Riel : ");
        jLabelF.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelF, gridBagConstraints);
        gridBagConstraints.gridy++;
        Riel = new JTextField(20);
        Riel.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Riel.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                    e.consume();  // if it's not a number, ignore the event
                }
            }
        });
        //firstbit.setPreferredSize (new Dimension(100,50));
        jPanel.add(Riel, gridBagConstraints);

        back = new JButton("Back");
        back.setFont(new Font("Segoe UI", Font.BOLD, 20));
        back.setBackground(new Color(255,255,255));
        back.setSize (new Dimension(100,50));
        jPanel.add(back, gridBagConstraints);
        gridBagConstraints.gridy++;

        submit = new JButton("Submit");
        submit.setFont(new Font("Segoe UI", Font.BOLD, 20));
        submit.setBackground(new Color(255,255,255));
        submit.setSize (new Dimension(100,50));
        jPanel.add(submit, gridBagConstraints);
        gridBagConstraints.gridy++;

    }

    private void output(){
        jPaneloutput = new JPanel();
        jPaneloutput.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.insets = new Insets(4, 4, 4, 4);
        gbc.gridx++;

        USD = new JLabel(" ");
        USD.setFont(new Font("Segoe UI", Font.BOLD, 20));
        USD.setBorder(new CompoundBorder(new TitledBorder("Result in USD") , new EmptyBorder(8, 0, 0, 0)));
        USD.setBackground(Color.GRAY);
        USD.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(USD,gbc);

        gbc.gridy++;
        gbc.gridx++;

        Euro = new JLabel(" ");
        Euro.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Euro.setBorder(new CompoundBorder(new TitledBorder("Result in Euro") , new EmptyBorder(8, 0, 0, 0)));
        Euro.setBackground(Color.GRAY);
        Euro.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(Euro,gbc);

        gbc.gridy++;
        gbc.gridx++;

        Frank = new JLabel(" ");
        Frank.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Frank.setBorder(new CompoundBorder(new TitledBorder("Result in Frank") , new EmptyBorder(8, 0, 0, 0)));
        Frank.setBackground(Color.GRAY);
        Frank.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(Frank,gbc);

        gbc.gridy++;
        gbc.gridx++;

        Pound = new JLabel(" ");
        Pound.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Pound.setBorder(new CompoundBorder(new TitledBorder("Result in Pound") , new EmptyBorder(8, 0, 0, 0)));
        Pound.setBackground(Color.GRAY);
        Pound.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(Pound,gbc);

        gbc.gridy++;
        gbc.gridx++;

        Baht = new JLabel(" ");
        Baht.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Baht.setBorder(new CompoundBorder(new TitledBorder("Result in Baht") , new EmptyBorder(8, 0, 0, 0)));
        Baht.setBackground(Color.GRAY);
        Baht.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(Baht,gbc);


        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
    }


    public  Riel_to_other(){

        input();
        output();

        back.addActionListener(e -> {
            if (e.getSource() == back){
                MoneyExchangeView moneyExchangeView = new MoneyExchangeView();
                moneyExchangeView.setVisible(true);
                this.setVisible(false);
            }
        });




        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 0.33;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(4, 4, 4, 4);

        add(jPanel , gbc);
        gbc.gridy++;
        add(jPaneloutput, gbc);
        gbc.gridy++;

        setLocation(300,300);
        setSize(1200,500);
        setTitle("MoneyExchange > Riel to Other");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public float getRiel() {
        return Float.parseFloat(Riel.getText());
    }

    public void setSubmit(ActionListener listenForCalcButton) {
        this.submit.addActionListener(listenForCalcButton);
    }

    public void setUSD(float solution) {
        this.USD.setText(Float.toString(solution));
    }

    public void setEuro(float solution) {
        this.Euro.setText(Float.toString(solution));
    }

    public void setFrank(float solution) {
        this.Frank.setText(Float.toString(solution));
    }

    public void setPound(float solution) {
        this.Pound.setText(Float.toString(solution));
    }

    public void setBaht(float solution) {
        this.Baht.setText(Float.toString(solution));
    }
    public void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }

    public void setkey(Riel_to_Other_Controller.keynumberonly keynumberonly) {
        keynumberonly.key(Riel);
    }
}


