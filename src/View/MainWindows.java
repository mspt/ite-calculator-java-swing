package View;

import Helper.assets.icons.Property;
import Controller.*;
import Model.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindows extends JFrame implements ActionListener{
    JPanel panel;
    //create Menu
    private JLabel jLabel;
    private JPanel jPanel;
    private JPanel jPanelButton;
    private JButton Arithmetic;

    public JButton BinaryArithmetic;

    private JButton MoneyExchange;

    private JButton Bitwise;
    private int top = 20, left = 20, bottom = 20, right = 20;
    private Insets i = new Insets(top, left, bottom, right);
    private void CreateTitle() {
        //create menu

        jLabel = new JLabel("ITE Calculator GUI");
        jLabel.setFont(new Font("Segoe UI", Font.BOLD, 52));
        jPanel = new JPanel(new FlowLayout());
        jPanel.add(jLabel);
    }

    private void ButtonLayout() {
        jPanelButton = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        Arithmetic = new JButton("Arithmetic");
        Arithmetic.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        Arithmetic.setOpaque(false);
        Arithmetic.setBackground(new Color(255,255,255));
        Arithmetic.setFocusPainted(false);
        Arithmetic.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder = new Property.RoundedBorder(new Color(0, 0, 0), 50);
        Border emptyBorder = BorderFactory.createEmptyBorder(
                Arithmetic.getBorder().getBorderInsets(Arithmetic).top,
                Arithmetic.getBorder().getBorderInsets(Arithmetic).left,
                Arithmetic.getBorder().getBorderInsets(Arithmetic).bottom,
                Arithmetic.getBorder().getBorderInsets(Arithmetic).right);
        Arithmetic.setBorder(BorderFactory.createCompoundBorder(Lineborder, emptyBorder));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 0;
        c.gridy = 0;
        jPanelButton.add(Arithmetic,c);


        Bitwise = new JButton("Bitwise");
        Bitwise.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        Bitwise.setOpaque(false);
        Bitwise.setBackground(new Color(255,255,255));
        Bitwise.setFocusPainted(false);
        Bitwise.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder1 = new Property.RoundedBorder(new Color(0, 0, 0), 50);
        Border emptyBorder1 = BorderFactory.createEmptyBorder(
                Bitwise.getBorder().getBorderInsets(Bitwise).top,
                Bitwise.getBorder().getBorderInsets(Bitwise).left,
                Bitwise.getBorder().getBorderInsets(Bitwise).bottom,
                Bitwise.getBorder().getBorderInsets(Bitwise).right);
        Bitwise.setBorder(BorderFactory.createCompoundBorder(Lineborder1, emptyBorder1));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 1;
        c.gridy = 0;
        jPanelButton.add(Bitwise,c);




        MoneyExchange = new JButton("Money Exchange");
        MoneyExchange.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        MoneyExchange.setOpaque(false);
        MoneyExchange.setBackground(new Color(255,255,255));
        MoneyExchange.setFocusPainted(false);
        MoneyExchange.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder2 = new Property.RoundedBorder(new Color(0, 0, 0), 50);
        Border emptyBorder2 = BorderFactory.createEmptyBorder(
                MoneyExchange.getBorder().getBorderInsets(MoneyExchange).top,
                MoneyExchange.getBorder().getBorderInsets(MoneyExchange).left,
                MoneyExchange.getBorder().getBorderInsets(MoneyExchange).bottom,
                MoneyExchange.getBorder().getBorderInsets(MoneyExchange).right);

        MoneyExchange.setBorder(BorderFactory.createCompoundBorder(Lineborder2, emptyBorder2));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 2;
        c.gridy = 0;
        jPanelButton.add(MoneyExchange,c);

        BinaryArithmetic = new JButton("Binary Arithmetic");
        BinaryArithmetic.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        BinaryArithmetic.setOpaque(false);
        BinaryArithmetic.setBackground(new Color(255,255,255));
        BinaryArithmetic.setFocusPainted(false);
        BinaryArithmetic.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder6 = new Property.RoundedBorder(new Color(0, 0, 0), 50);
        Border emptyBorder6 = BorderFactory.createEmptyBorder(
                BinaryArithmetic.getBorder().getBorderInsets(BinaryArithmetic).top,
                BinaryArithmetic.getBorder().getBorderInsets(BinaryArithmetic).left,
                BinaryArithmetic.getBorder().getBorderInsets(BinaryArithmetic).bottom,
                BinaryArithmetic.getBorder().getBorderInsets(BinaryArithmetic).right);
        BinaryArithmetic.setBorder(BorderFactory.createCompoundBorder(Lineborder6, emptyBorder6));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 1;
        c.gridy = 2;
        jPanelButton.add(BinaryArithmetic,c);

    }

    public MainWindows() {
        panel = new JPanel();
        JPanel jPanelMainWindows = new JPanel();

        CreateTitle();
        ButtonLayout();

        Arithmetic.addActionListener(e -> {
            if (e.getSource() == Arithmetic){
                Arithmetic arithmaticViewController = new Arithmetic();
                ArithmeticModel arithmeticModel = new ArithmeticModel();
                arithmaticViewController.setVisible(true);
                this.setVisible(false);
            }
        });

        Bitwise.addActionListener(e -> {
            if (e.getSource() == Bitwise){
                BitwiseView bitwiseView = new BitwiseView();
                BitwiseModel bitwiseModel = new BitwiseModel();
                BitwiseController bitwiseController = new BitwiseController(bitwiseView,bitwiseModel);
                bitwiseView.setVisible(true);
                this.setVisible(false);
            }
        });

        MoneyExchange.addActionListener(e -> {
            if (e.getSource() == MoneyExchange){
                MoneyExchangeView moneyExchangeView = new MoneyExchangeView();
                moneyExchangeView.setVisible(true);
                this.setVisible(false);
            }
        });

        BinaryArithmetic.addActionListener(e -> {
            if (e.getSource() == BinaryArithmetic){
                BinaryarithmeticView binaryarithmeticView = new BinaryarithmeticView();
                BinaryArithmeticModel binaryArithmeticModel = new BinaryArithmeticModel();
                BinaryArithmeticController binaryArithmeticController = new BinaryArithmeticController(binaryarithmeticView,binaryArithmeticModel);
                binaryarithmeticView.setVisible(true);
                this.setVisible(false);
            }
        });

        jPanelMainWindows.add(jPanel);
        jPanelMainWindows.add(jPanelButton);
        jPanelMainWindows.setLayout(new BoxLayout(jPanelMainWindows, BoxLayout.PAGE_AXIS));
        add(jPanelMainWindows);

        setSize(1200, 800);
        setTitle("First GUI");
        setLocation(400, 100);
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    public void setArithmetic(JButton arithmetic) {
        Arithmetic = arithmetic;
    }
}
