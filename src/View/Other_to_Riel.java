package View;

import Controller.Other_to_Riel_Controller;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Other_to_Riel extends JFrame {

    private JPanel jPanel;
    private JPanel jPaneloutput;

    private JLabel jLabelUSD;
    private JLabel jLabelEuro;
    private JLabel jLabelFrank;
    private JLabel jLabelPound;
    private JLabel jLabelBaht;


    private JTextField USD;
    private JTextField Euro;
    private JTextField Frank;
    private JTextField Pound;
    private JTextField Baht;


    private JButton USDButton;
    private JButton EuroButton;
    private JButton FrankButton;
    private JButton PoundButton;
    private JButton BahtButton;

    private JButton back;

    private JLabel USDtoRiel;
    private JLabel EUROtoRiel;
    private JLabel FranktoRiel;
    private JLabel PoundtoRiel;
    private JLabel BahttoRiel;


    private int top = 20, left = 20, bottom = 20, right = 20;
    private Insets i = new Insets(top, left, bottom, right);

    private void input() {
        jPanel = new JPanel();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = GridBagConstraints.WEST;

        jLabelUSD = new JLabel("Input in USD ");
        jLabelUSD.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelUSD, gridBagConstraints);
        gridBagConstraints.gridy++;
        USD = new JTextField(20);
        USD.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(USD, gridBagConstraints);



        USDButton = new JButton("Submit");
        USDButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        USDButton.setSize (new Dimension(100,50));
        USDButton.setBackground(Color.white);
        jPanel.add(USDButton, gridBagConstraints);
        gridBagConstraints.gridy++;

        jLabelEuro = new JLabel("Input in Euro ");
        jLabelEuro.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelEuro, gridBagConstraints);
        gridBagConstraints.gridy++;
        Euro = new JTextField(20);
        Euro.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(Euro, gridBagConstraints);



        EuroButton = new JButton("Submit");
        EuroButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        EuroButton.setSize (new Dimension(100,50));
        EuroButton.setBackground(Color.white);
        jPanel.add(EuroButton, gridBagConstraints);
        gridBagConstraints.gridy++;


        jLabelFrank = new JLabel("Input in Frank ");
        jLabelFrank.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelFrank, gridBagConstraints);
        gridBagConstraints.gridy++;
        Frank = new JTextField(20);
        Frank.setFont(new Font("Segoe UI", Font.BOLD, 20));
        //firstbit.setPreferredSize (new Dimension(100,50));
        jPanel.add(Frank, gridBagConstraints);



        FrankButton = new JButton("Submit");
        FrankButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        FrankButton.setSize (new Dimension(100,50));
        FrankButton.setBackground(Color.white);
        jPanel.add(FrankButton, gridBagConstraints);
        gridBagConstraints.gridy++;



        jLabelPound = new JLabel("Input in Pound ");
        jLabelPound.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelPound, gridBagConstraints);
        gridBagConstraints.gridy++;
        Pound = new JTextField(20);
        Pound.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(Pound, gridBagConstraints);



        PoundButton = new JButton("Submit");
        PoundButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        PoundButton.setSize (new Dimension(100,50));
        PoundButton.setBackground(Color.white);
        jPanel.add(PoundButton, gridBagConstraints);
        gridBagConstraints.gridy++;

        jLabelBaht = new JLabel("Input in Baht ");
        jLabelBaht.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelBaht, gridBagConstraints);
        gridBagConstraints.gridy++;
        Baht = new JTextField(20);
        Baht.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(Baht, gridBagConstraints);



        BahtButton = new JButton("Submit");
        BahtButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        BahtButton.setSize (new Dimension(100,50));
        BahtButton.setBackground(Color.white);
        jPanel.add(BahtButton, gridBagConstraints);
        gridBagConstraints.gridy++;


        back = new JButton("Back");
        back.setFont(new Font("Segoe UI", Font.BOLD, 20));
        back.setSize (new Dimension(100,50));
        back.setBackground(Color.white);
        jPanel.add(back, gridBagConstraints);
        gridBagConstraints.gridy++;

    }

    private void output(){

        jPaneloutput = new JPanel();
        jPaneloutput.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.insets = new Insets(4, 4, 4, 4);
        gbc.gridx++;

        USDtoRiel = new JLabel(" ");
        USDtoRiel.setFont(new Font("Segoe UI", Font.BOLD, 20));
        USDtoRiel.setBorder(new CompoundBorder(new TitledBorder("Result USD to Riel") , new EmptyBorder(8, 0, 0, 0)));
        USDtoRiel.setBackground(Color.GRAY);
        USDtoRiel.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(USDtoRiel,gbc);

        gbc.gridy++;
        gbc.gridx++;

        EUROtoRiel = new JLabel(" ");
        EUROtoRiel.setFont(new Font("Segoe UI", Font.BOLD, 20));
        EUROtoRiel.setBorder(new CompoundBorder(new TitledBorder("Result Euro to Riel") , new EmptyBorder(8, 0, 0, 0)));
        EUROtoRiel.setBackground(Color.GRAY);
        EUROtoRiel.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(EUROtoRiel,gbc);

        gbc.gridy++;
        gbc.gridx++;

        FranktoRiel = new JLabel(" ");
        FranktoRiel.setFont(new Font("Segoe UI", Font.BOLD, 20));
        FranktoRiel.setBorder(new CompoundBorder(new TitledBorder("Result Frank to Riel") , new EmptyBorder(8, 0, 0, 0)));
        FranktoRiel.setBackground(Color.GRAY);
        FranktoRiel.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(FranktoRiel,gbc);

        gbc.gridy++;
        gbc.gridx++;

        PoundtoRiel = new JLabel(" ");
        PoundtoRiel.setFont(new Font("Segoe UI", Font.BOLD, 20));
        PoundtoRiel.setBorder(new CompoundBorder(new TitledBorder("Result Pound to Riel") , new EmptyBorder(8, 0, 0, 0)));
        PoundtoRiel.setBackground(Color.GRAY);
        PoundtoRiel.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(PoundtoRiel,gbc);

        gbc.gridy++;
        gbc.gridx++;

        BahttoRiel = new JLabel(" ");
        BahttoRiel.setFont(new Font("Segoe UI", Font.BOLD, 20));
        BahttoRiel.setBorder(new CompoundBorder(new TitledBorder("Result Baht to Riel") , new EmptyBorder(8, 0, 0, 0)));
        BahttoRiel.setBackground(Color.GRAY);
        BahttoRiel.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(BahttoRiel,gbc);


        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
    }



    public  Other_to_Riel(){

        input();
        output();

        back.addActionListener(e -> {
            if (e.getSource() == back){
                MoneyExchangeView moneyExchangeView = new MoneyExchangeView();
                moneyExchangeView.setVisible(true);
                this.setVisible(false);
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 0.33;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(4, 4, 4, 4);

        add(jPanel , gbc);
        gbc.gridy++;
        add(jPaneloutput, gbc);
        gbc.gridy++;

        setLocation(500,0);
        setSize(700,900);
        setTitle("MoneyExchange > Riel to Other");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public float getUSD() {
        return Float.parseFloat(USD.getText());
    }

    public float getEuro() {
        return Float.parseFloat(Euro.getText());
    }

    public float getFrank() {
        return Float.parseFloat(Frank.getText());
    }

    public float getPound() {
        return Float.parseFloat(Pound.getText());
    }

    public float getBaht() {
        return Float.parseFloat(Baht.getText());
    }

    public void setUSDButton(ActionListener listenForCalcButton1) {
        this.USDButton.addActionListener(listenForCalcButton1);
    }

    public void setEuroButton(ActionListener listenForCalcButton2) {
        this.EuroButton.addActionListener(listenForCalcButton2);
    }

    public void setFrankButton(ActionListener listenForCalcButton3) {
        this.FrankButton.addActionListener(listenForCalcButton3);
    }

    public void setPoundButton(ActionListener listenForCalcButton4) {
        this.PoundButton.addActionListener(listenForCalcButton4);
    }

    public void setBahtButton(ActionListener listenForCalcButton5) {
        this.BahtButton.addActionListener(listenForCalcButton5);
    }


    public void setUSDtoRiel(float solution) {
        this.USDtoRiel.setText(Float.toString(solution));
    }

    public void setEUROtoRiel(float solution) {
        this.EUROtoRiel.setText(Float.toString(solution));
    }

    public void setFranktoRiel(float solution) {
        this.FranktoRiel.setText(Float.toString(solution));
    }

    public void setPoundtoRiel(float solution) {
        this.PoundtoRiel.setText(Float.toString(solution));
    }

    public void setBahttoRiel(float solution) {
        this.BahttoRiel.setText(Float.toString(solution));
    }

    public void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }

    public void setkey(Other_to_Riel_Controller.keynumberonly keynumberonly) {
        keynumberonly.key(USD);
        keynumberonly.key(Frank);
        keynumberonly.key(Pound);
        keynumberonly.key(Baht);
        keynumberonly.key(Euro);
    }
}
