package View;

import Controller.BitwiseController;
import Model.ArithmeticModel;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class BitwiseView extends JFrame  {

    private JPanel jPanel;
    private JPanel jPaneloutput;

    private JLabel jLabelF;
    private JLabel jLabelS;

    private JTextField firstbit;
    private JTextField secondbit;

    private JButton ANDButton;
    private JButton ORButton;
    private JButton XORButton;
    private JButton LeftButton;
    private JButton RightButton;
    private JButton back;

    private JLabel ANDOUT;
    private JLabel OROUT;
    private JLabel XOROUT;
    private JLabel LeftShiftOUT;
    private JLabel RightShiftOUT;
    private JLabel InversionOUT;


    private int top = 20, left = 20, bottom = 20, right = 20;
    private Insets i = new Insets(top, left, bottom, right);

    private void input() {
        jPanel = new JPanel();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = GridBagConstraints.WEST;

        jLabelF = new JLabel("Input First Bit : ");
        jLabelF.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelF, gridBagConstraints);
        gridBagConstraints.gridy++;
        firstbit = new JTextField(20);
        firstbit.setFont(new Font("Segoe UI", Font.BOLD, 20));
        //firstbit.setPreferredSize (new Dimension(100,50));
        jPanel.add(firstbit, gridBagConstraints);


        gridBagConstraints.gridx++;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;

        jLabelS = new JLabel("Input Second Bit : ");
        jLabelS.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelS, gridBagConstraints);
        gridBagConstraints.gridy++;
        secondbit = new JTextField(20);
        secondbit.setFont(new Font("Segoe UI", Font.BOLD, 20));
        //secondbit.setPreferredSize (new Dimension(100,50));
        jPanel.add(secondbit, gridBagConstraints);

        gridBagConstraints.gridx++;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;


        back = new JButton("Back");
        back.setFont(new Font("Segoe UI", Font.BOLD, 20));
        back.setSize (new Dimension(100,50));
        back.setBackground(Color.white);
        jPanel.add(back, gridBagConstraints);
        gridBagConstraints.gridy++;

        ANDButton = new JButton("AND");
        ANDButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        ANDButton.setSize (new Dimension(100,50));
        ANDButton.setBackground(Color.white);
        jPanel.add(ANDButton, gridBagConstraints);
        gridBagConstraints.gridy++;

        ORButton = new JButton("OR");
        ORButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        ORButton.setSize (new Dimension(100,50));
        ORButton.setBackground(Color.white);
        jPanel.add(ORButton, gridBagConstraints);
        gridBagConstraints.gridy++;

        XORButton = new JButton("XOR");
        XORButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        XORButton.setSize (new Dimension(100,50));
        XORButton.setBackground(Color.white);
        jPanel.add(XORButton, gridBagConstraints);
        gridBagConstraints.gridy++;

        LeftButton = new JButton("Left Shift");
        LeftButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        LeftButton.setSize (new Dimension(100,50));
        LeftButton.setBackground(Color.white);
        jPanel.add(LeftButton, gridBagConstraints);
        gridBagConstraints.gridy++;

        RightButton = new JButton("Right Shift");
        RightButton.setFont(new Font("Segoe UI", Font.BOLD, 20));
        RightButton.setSize (new Dimension(100,50));
        RightButton.setBackground(Color.white);
        jPanel.add(RightButton, gridBagConstraints);
        gridBagConstraints.gridy++;

    }

    private void output(){
        jPaneloutput = new JPanel();
        jPaneloutput.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.insets = new Insets(4, 4, 4, 4);
        gbc.gridx++;

        ANDOUT = new JLabel(" ");
        ANDOUT.setFont(new Font("Segoe UI", Font.BOLD, 20));
        ANDOUT.setBorder(new CompoundBorder(new TitledBorder("Result of AND Gate") , new EmptyBorder(8, 0, 0, 0)));
        ANDOUT.setBackground(Color.GRAY);
        ANDOUT.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(ANDOUT,gbc);
        gbc.gridy++;
        gbc.gridx++;

        OROUT = new JLabel(" ");
        OROUT.setFont(new Font("Segoe UI", Font.BOLD, 20));
        OROUT.setBorder(new CompoundBorder(new TitledBorder("Result of OR Gate") , new EmptyBorder(8, 0, 0, 0)));
        OROUT.setBackground(Color.GRAY);
        OROUT.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(OROUT,gbc);
        gbc.gridy++;
        gbc.gridx++;

        XOROUT = new JLabel(" ");
        XOROUT.setFont(new Font("Segoe UI", Font.BOLD, 20));
        XOROUT.setBorder(new CompoundBorder(new TitledBorder("Result of XOR Gate") , new EmptyBorder(8, 0, 0, 0)));
        XOROUT.setBackground(Color.GRAY);
        XOROUT.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(XOROUT,gbc);
        gbc.gridy++;
        gbc.gridx++;

        LeftShiftOUT = new JLabel(" ");
        LeftShiftOUT.setFont(new Font("Segoe UI", Font.BOLD, 20));
        LeftShiftOUT.setBorder(new CompoundBorder(new TitledBorder("Result of LeftShift") , new EmptyBorder(8, 0, 0, 0)));
        LeftShiftOUT.setBackground(Color.GRAY);
        LeftShiftOUT.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(LeftShiftOUT,gbc);

        gbc.gridy++;
        gbc.gridx++;
        RightShiftOUT = new JLabel(" ");
        RightShiftOUT.setFont(new Font("Segoe UI", Font.BOLD, 20));
        RightShiftOUT.setBorder(new CompoundBorder(new TitledBorder("Result of RightShift") , new EmptyBorder(8, 0, 0, 0)));
        RightShiftOUT.setBackground(Color.GRAY);
        RightShiftOUT.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(RightShiftOUT,gbc);


        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
    }

    public BitwiseView(){
        input();
        output();

        back.addActionListener(e -> {
            if (e.getSource() == back){
                MainWindows mainWindows = new MainWindows();
                mainWindows.setVisible(true);
                this.setVisible(false);
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 0.33;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(4, 4, 4, 4);

        add(jPanel , gbc);
        gbc.gridy++;
        add(jPaneloutput, gbc);
        gbc.gridy++;

        setLocation(300,300);
        setSize(1100,500);
        setTitle("Bitwise");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public int getFirstbit() {
        return Integer.parseInt(firstbit.getText());
    }

    public int getSecondbit() {
        return Integer.parseInt(secondbit.getText());
    }


    public void setANDButton(ActionListener listenForCalcButton1) {
        this.ANDButton.addActionListener(listenForCalcButton1);
    }

    public void setORButton(ActionListener listenForCalcButton2) {
        this.ORButton.addActionListener(listenForCalcButton2);
    }

    public void setXORButton(ActionListener listenForCalcButton3) {
        this.XORButton.addActionListener(listenForCalcButton3);
    }

    public void setLeftButton(ActionListener listenForCalcButton4) {
        this.LeftButton.addActionListener(listenForCalcButton4);
    }

    public void setRightButton(ActionListener listenForCalcButto5) {
        this.RightButton.addActionListener(listenForCalcButto5);
    }

    public void setANDOUT(int solution) {
        this.ANDOUT.setText(Integer.toString(solution));
    }

    public void setOROUT(int solution) {
        this.OROUT.setText(Integer.toString(solution));
    }

    public void setXOROUT(int solution) {
        this.XOROUT.setText(Integer.toString(solution));
    }

    public void setLeftShiftOUT(int solution) {
        this.LeftShiftOUT.setText(Integer.toString(solution));
    }

    public void setRightShiftOUT(int solution) {
        this.RightShiftOUT.setText(Integer.toString(solution));
    }

    public void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }

    public void setkey(BitwiseController.keynumberonly keynumberonly) {
        keynumberonly.key(firstbit);
        keynumberonly.key(secondbit);
    }
}