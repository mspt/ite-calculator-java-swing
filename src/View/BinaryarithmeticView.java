package View;

import Controller.BinaryArithmeticController;
import Controller.Riel_to_Other_Controller;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BinaryarithmeticView extends JFrame {

    private JPanel jPanel;
    private JPanel jPaneloutput;

    private JLabel jLabelF;
    private JLabel jLabelS;

    private JTextField firstbit;
    private JTextField secondbit;

    private JButton submit;
    private JButton back;

    private JLabel Addition;
    private JLabel Subtraction ;
    private JLabel MultiplyBinary;
    private JLabel DivideBinary;



    private int top = 20, left = 20, bottom = 20, right = 20;
    private Insets i = new Insets(top, left, bottom, right);

    private void input() {
        jPanel = new JPanel();
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = GridBagConstraints.WEST;

        jLabelF = new JLabel("Input First Binary : ");
        jLabelF.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelF, gridBagConstraints);
        gridBagConstraints.gridy++;
        firstbit = new JTextField(20);
        firstbit.setFont(new Font("Segoe UI", Font.BOLD, 20));

        jPanel.add(firstbit, gridBagConstraints);


        gridBagConstraints.gridx++;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;

        jLabelS = new JLabel("Input Second Binary : ");
        jLabelS.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(jLabelS, gridBagConstraints);
        gridBagConstraints.gridy++;
        secondbit = new JTextField(20);
        secondbit.setFont(new Font("Segoe UI", Font.BOLD, 20));
        jPanel.add(secondbit, gridBagConstraints);

        gridBagConstraints.gridx++;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;


        back = new JButton("Back");
        back.setFont(new Font("Segoe UI", Font.BOLD, 20));
        back.setSize (new Dimension(100,50));
        back.setBackground(Color.white);
        jPanel.add(back, gridBagConstraints);
        gridBagConstraints.gridy++;

        submit = new JButton("Submit");
        submit.setFont(new Font("Segoe UI", Font.BOLD, 20));
        submit.setSize (new Dimension(100,50));
        submit.setBackground(Color.white);
        jPanel.add(submit, gridBagConstraints);
        gridBagConstraints.gridy++;

    }

    private void output(){
        jPaneloutput = new JPanel();
        jPaneloutput.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.insets = new Insets(4, 4, 4, 4);
        gbc.gridx++;

        Addition = new JLabel(" ");
        Addition.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Addition.setBorder(new CompoundBorder(new TitledBorder("Addition Binary") , new EmptyBorder(8, 0, 0, 0)));
        Addition.setBackground(Color.GRAY);
        Addition.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(Addition,gbc);
        gbc.gridy++;
        gbc.gridx++;

        Subtraction = new JLabel(" ");
        Subtraction.setFont(new Font("Segoe UI", Font.BOLD, 20));
        Subtraction.setBorder(new CompoundBorder(new TitledBorder("Subtraction Binary") , new EmptyBorder(8, 0, 0, 0)));
        Subtraction.setBackground(Color.GRAY);
        Subtraction.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(Subtraction,gbc);
        gbc.gridy++;
        gbc.gridx++;




        MultiplyBinary = new JLabel(" ");
        MultiplyBinary.setFont(new Font("Segoe UI", Font.BOLD, 20));
        MultiplyBinary.setBorder(new CompoundBorder(new TitledBorder("Multiply Binary") , new EmptyBorder(8, 0, 0, 0)));
        MultiplyBinary.setBackground(Color.GRAY);
        MultiplyBinary.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(MultiplyBinary,gbc);

        gbc.gridy++;
        gbc.gridx++;

        DivideBinary = new JLabel(" ");
        DivideBinary.setFont(new Font("Segoe UI", Font.BOLD, 20));
        DivideBinary.setBorder(new CompoundBorder(new TitledBorder("Divide Binary") , new EmptyBorder(8, 0, 0, 0)));
        DivideBinary.setBackground(Color.GRAY);
        DivideBinary.setPreferredSize(new Dimension(300,100));
        jPaneloutput.add(DivideBinary,gbc);






        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
    }

    public  BinaryarithmeticView(){

        input();
        output();

        back.addActionListener(e -> {
            if (e.getSource() == back){
                MainWindows mainWindows = new MainWindows();
                mainWindows.setVisible(true);
                this.setVisible(false);
            }
        });

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 0.33;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(4, 4, 4, 4);

        add(jPanel , gbc);
        gbc.gridy++;
        add(jPaneloutput, gbc);
        gbc.gridy++;

        setLocation(300,100);
        setSize(1200,500);
        setTitle("Binary Arithmetic");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
    public int getFirstbit() {
        return Integer.parseInt(firstbit.getText());
    }

    public int getSecondbit() {
        return Integer.parseInt(secondbit.getText());
    }

    public void setSubmit(ActionListener listenForCalcButton) {
        this.submit.addActionListener(listenForCalcButton);
    }

    public void setAddition(String solution) {
        this.Addition.setText(solution);
    }

    public void setSubtraction(String solution) {
        this.Subtraction.setText(solution);
    }

    public void setMultiplyBinary(int solution) {
        this.MultiplyBinary.setText(Integer.toString(solution));
    }

    public void setDivideBinary(int solution) {
        this.DivideBinary.setText(Integer.toString(solution));
    }
    public void displayErrorMessage(String errorMessage){
        JOptionPane.showMessageDialog(this, errorMessage);
    }


    public void setkey(BinaryArithmeticController.keynumberonly keynumberonly) {
        keynumberonly.key(firstbit);
        keynumberonly.key(secondbit);
    }
}
