package View;

import Controller.*;
import Model.ArithmeticModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class Arithmetic  extends JFrame {
    public JButton back;
    public  JTextField display1;
    public  JTextField display2 = new JTextField();
    private  JTextField display;

    public JTextField getDisplay() {
        return display;
    }

    private JTextField makeDisplay () {
        display = new JTextField();
        display.setEditable(false);
        display.setPreferredSize(new Dimension(16,30));
        display.setFont(new Font(Font.DIALOG,Font.PLAIN,25));
        display.setEditable(false);
        return display;
    }

    public  JButton dotButton;
    public  String str = "";

    ArithmeticController controller;
    ArithmeticModel model = new ArithmeticModel();
    createButton createButton;

    public Arithmetic(){


        makeDisplay();
        createButton = new createButton();
        setLayout( new BorderLayout());
        setTitle("Arithmetic");
        setLocation(800, 250);
        //setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);

        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel textFieldPanel = new JPanel(new BorderLayout());
        JPanel addOnPanel = new JPanel(new FlowLayout());
        JPanel topPanel = new JPanel(new BorderLayout());
        Font myFont = new Font("ArialBlack",Font.PLAIN,20);

        add(mainPanel);
        mainPanel.setBackground(Color.BLACK);
        topPanel.setBackground(Color.BLACK);


        display1 = new JTextField(16);
        display1.setPreferredSize(new Dimension(16,50));
        display1.setHorizontalAlignment((int) display1.RIGHT);
        display2.setHorizontalAlignment((int) display1.RIGHT);
        display2.setPreferredSize(new Dimension(16,50));
        display1.setFont(myFont);
        display2.setFont(myFont);
        display1.setEditable(false);
        display1.setBackground(Color.white);
        display1.setBorder(null);
        display2.setEditable(false);

        JPanel textFieldPanel2 = new JPanel(new BorderLayout());
        textFieldPanel2.setBackground(Color.BLACK);
        textFieldPanel2.add(display1, BorderLayout.NORTH);
        textFieldPanel2.add(display2, BorderLayout.SOUTH);

        textFieldPanel.add(textFieldPanel2);

        topPanel.add(textFieldPanel, BorderLayout.NORTH);
        topPanel.add(addOnPanel, BorderLayout.SOUTH);

        mainPanel.add(topPanel, BorderLayout.NORTH);

        JPanel keyPad3 = new JPanel();
        keyPad3.setLayout(new GridLayout(4,4,2,2));
        keyPad3.setBackground(Color.white);
        String plusMinus = "\u00B1";
        String[] elements = new String []{"7","8","9","/","4","5","6","*","1","2","3","-","0",".",plusMinus,"+"};

        for (int i = 0 ;i <= 15 ; i++){
            if (i == 14){
                keyPad3.add(createButton(elements[i],elements[i],Color.black,Color.white, new ArithmeticController(this,model)));
            } else if (i==13){
                dotButton = createButton(elements[i],elements[i],Color.black,Color.white,new ArithmeticController(this,model));
                keyPad3.add(dotButton);

            }
            else if (i == 3 || i == 7 || i == 11 || i == 15 ){
                keyPad3.add(createButton(elements[i],elements[i],Color.black,Color.white,new ArithmeticController(this,model)));
            } else {
                keyPad3.add(createButton(elements[i],elements[i],Color.black,Color.white,new ArithmeticController(this,model)));
            }
        }


        JPanel keyPad2 = new JPanel(new GridLayout(0,1,2,2));
        keyPad2.add(createButton("C","C",Color.BLACK, Color.white,new ArithmeticController(this,model)));
        keyPad2.setFont(myFont);
        keyPad2.add(createButton("=","=",Color.BLACK,Color.white,new ArithmeticController(this,model)));

        JPanel keyPad = new JPanel(new BorderLayout());
        keyPad.add(keyPad2,BorderLayout.EAST);
        keyPad.add(keyPad3,BorderLayout.CENTER);

        mainPanel.add(keyPad, BorderLayout.CENTER);
        back = new JButton("Back");
        back.setPreferredSize(new Dimension(100,50));
        back.setBackground(Color.white);

        back.addActionListener(e -> {
            if (e.getSource() == back){
                MainWindows mainWindows = new MainWindows();
                mainWindows.setVisible(true);
                this.setVisible(false);
            }
        });


        mainPanel.add(back, BorderLayout.SOUTH);

    }
    private JButton createButton(String text, String ac, Color fg, Color bg, ActionListener handler){
        JButton button = new JButton(text);
        button.setForeground(fg);
        button.setBackground(bg);

        if (ac != null){
            button.setActionCommand(ac);
        }

        button.setFont(new Font( button.getFont().getName(),  button.getFont().getStyle(), 20));
        button.addActionListener(handler);

        if ("=".equals(text)){
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS,0), text);
        } else if ("+".equals(text)){
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS,0), text);
        }else if ("-".equals(text)){
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS,0), text);
        } else if ("*".equals(text)){
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_MULTIPLY,0), text);
        } else if ("/".equals(text)){
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DIVIDE,0),text);
        } else {
            button.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(text),ac);
        }

        button.getActionMap().put(text, (Action) handler);
        return button;

    }



    public void setStr(String str) {
        this.str = str;
    }

    public void setDotButton(JButton dotButton) {
        this.dotButton = dotButton;
    }

    public void setDisplay2(JTextField display2) {
        this.display2 = display2;
    }

    public void setDisplay1(JTextField display1) {
        this.display1 = display1;
    }

    public JTextField getDisplay1() {
        return display1;
    }

    public JTextField getDisplay2() {
        return display2;
    }
}

