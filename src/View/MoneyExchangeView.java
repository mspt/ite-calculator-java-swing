package View;

import Controller.Other_to_Riel_Controller;
import Controller.Riel_to_Other_Controller;
import Helper.assets.icons.Property;
import Model.ArithmeticModel;
import Model.Other_to_Riel_Model;
import Model.Riel_to_Other_Model;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class MoneyExchangeView extends JFrame {

    private JLabel jLabel;
    private JPanel jPanel;
    private JPanel jPanelTitle;
    private JButton back;
    private JPanel jPanelButton;

    JButton riel_to_other;
    JButton other_to_riel;

    private int top = 20, left = 20, bottom = 20, right = 20;
    private Insets i = new Insets(top, left, bottom, right);

    private void CreateTitle() {
        //create menu
        jPanelTitle = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        jLabel = new JLabel("MoneyExchange");
        jLabel.setFont(new Font("Segoe UI", Font.BOLD, 52));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 0;
        c.gridy = 0;
        jPanelTitle.add(jLabel,c);


        //create menu
        back = new JButton("Back");
        back.setOpaque(false);
        back.setBackground(new Color(255,255,255));
        back.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        back.setFocusPainted(false);
        back.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder7 = new Property.RoundedBorder(new Color(0, 0, 0), 20);
        Border emptyBorder7 = BorderFactory.createEmptyBorder(
                back.getBorder().getBorderInsets(back).top,
                back.getBorder().getBorderInsets(back).left,
                back.getBorder().getBorderInsets(back).bottom,
                back.getBorder().getBorderInsets(back).right);
        back.setBorder(BorderFactory.createCompoundBorder(Lineborder7, emptyBorder7));
        back.setBounds(100,30,110,50);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 0;
        c.gridy = 1;
        jPanelTitle.add(back,c);

    }

    private void ButtonLayout() {

        jPanelButton = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        riel_to_other = new JButton("Riel to other");
        riel_to_other.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        riel_to_other.setOpaque(false);
        riel_to_other.setBackground(new Color(255,255,255));
        riel_to_other.setFocusPainted(false);
        riel_to_other.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder = new Property.RoundedBorder(new Color(0, 0, 0), 50);
        Border emptyBorder = BorderFactory.createEmptyBorder(
                riel_to_other.getBorder().getBorderInsets(riel_to_other).top,
                riel_to_other.getBorder().getBorderInsets(riel_to_other).left,
                riel_to_other.getBorder().getBorderInsets(riel_to_other).bottom,
                riel_to_other.getBorder().getBorderInsets(riel_to_other).right);
        riel_to_other.setBorder(BorderFactory.createCompoundBorder(Lineborder, emptyBorder));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 0;
        c.gridy = 0;
        jPanelButton.add(riel_to_other,c);

        other_to_riel = new JButton("Other to Riel");
        other_to_riel.setFont(new Font("Segoe UI", Font.PLAIN, 30));
        other_to_riel.setOpaque(false);
        other_to_riel.setBackground(new Color(255,255,255));
        other_to_riel.setFocusPainted(false);
        other_to_riel.setForeground(new Color(1, 1, 1));
        Property.RoundedBorder Lineborder1 = new Property.RoundedBorder(new Color(0, 0, 0), 50);
        Border emptyBorder1 = BorderFactory.createEmptyBorder(
                other_to_riel.getBorder().getBorderInsets(other_to_riel).top,
                other_to_riel.getBorder().getBorderInsets(other_to_riel).left,
                other_to_riel.getBorder().getBorderInsets(other_to_riel).bottom,
                other_to_riel.getBorder().getBorderInsets(other_to_riel).right);
        other_to_riel.setBorder(BorderFactory.createCompoundBorder(Lineborder1, emptyBorder1));
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = i;
        c.gridx = 1;
        c.gridy = 0;
        jPanelButton.add(other_to_riel,c);

    }

    MoneyExchangeView(){
        JPanel jPanelMoney = new JPanel();
        CreateTitle();
        ButtonLayout();
        jPanelMoney.setLayout(new BoxLayout(jPanelMoney, BoxLayout.PAGE_AXIS));
        jPanelMoney.add(jPanelTitle);
        jPanelMoney.add(jPanelButton);

        add(jPanelMoney);

        back.addActionListener(e -> {
            if (e.getSource() == back){
                MainWindows mainWindows = new MainWindows();
                mainWindows.setVisible(true);
                this.setVisible(false);
            }
        });

        riel_to_other.addActionListener(e -> {
            if (e.getSource() == riel_to_other){
                Riel_to_other riel_to_otherView = new Riel_to_other();
                Riel_to_Other_Model riel_to_other_model = new Riel_to_Other_Model();
                Riel_to_Other_Controller riel_to_other_controller = new Riel_to_Other_Controller(riel_to_otherView,riel_to_other_model);
                riel_to_otherView.setVisible(true);
                this.setVisible(false);
            }
        });

        other_to_riel.addActionListener(e -> {
            if (e.getSource() == other_to_riel){
                Other_to_Riel other_to_riel = new Other_to_Riel();
                Other_to_Riel_Model other_to_riel_model = new Other_to_Riel_Model();
                Other_to_Riel_Controller other_to_riel_controller = new Other_to_Riel_Controller(other_to_riel,other_to_riel_model);
                other_to_riel.setVisible(true);
                this.setVisible(false);
            }
        });


        setSize(800,500);
        setTitle("MoneyExchange");
        setLocation(600,300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
