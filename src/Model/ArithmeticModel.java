package Model;

import java.text.DecimalFormat;

public class ArithmeticModel {


    private String operand1= "";
    private String operand2= "";
    private String mode= ".00";
    private String arithmeticOp = "";
    private boolean errorState = false;

    public void setOperand1(String op){ //sets operand1
        this.operand1 = op;
    }
    public void setOperand2(String op){ // sets operand2
        this.operand2 = op;
    }

    public String getOperand1() { //gets operand1
        return operand1;
    }
    public String getOperand2() { // gets operand2
        return operand2;
    }
    public String getTotal() { // get the total from calculate
        return arithmeticOp;
    }

    public boolean getErrorState(){ // get the error state
        return errorState;
    }
    public void setErrorState(boolean state){ // set the error state
        this.errorState = state;

    }
    public void setMode(String mode){ // sets mode
        this.mode = mode;
    }
    public String getMode(){ // get mode
        return mode;
    }

    public void setArithmeticOp(String arithmeticOp) { // set arithmetic operation (/, *, -, +)
        this.arithmeticOp = arithmeticOp;
    }

    private float calculate(){
        if (operand1.isEmpty()){
            operand1 = "0";
        }
        if(operand2.isEmpty()) {
            this.operand2 = this.operand1;
        }
        if (arithmeticOp.isEmpty()){
            return converToFloat(operand2);
        }

        switch(arithmeticOp){

            case "+": return converToFloat(operand1) + converToFloat(operand2);
            case "-": return converToFloat(operand1) - converToFloat(operand2);
            case "*": return converToFloat(operand1) * converToFloat(operand2);
            case "/": return converToFloat(operand1) / converToFloat(operand2);
            default :
        }
        return 0;
    }

    public String precesion(){
        DecimalFormat format;

        if (mode.equals(".0")){
            format = new DecimalFormat("#0.0");
            return format.format(calculate());

        } else if (mode.equals(".00")){
            format = new DecimalFormat("#0.00") ;
            return format.format(calculate() ) ;
        } else if (("Int".equals(mode))){
            return String.valueOf(Math.round(calculate()));
        } else if (mode.equals("Sci")){
            format = new DecimalFormat("0.#####E00");
            return format.format(calculate());
        }
        return "";
    }

    public void reset(){
        operand1 = "";
        operand2 = "";
        arithmeticOp = "";
    }

    public float converToFloat (String n){ // converts strings to float
        return Float.parseFloat(n);
    }
}
