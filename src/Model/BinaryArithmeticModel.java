package Model;

public class BinaryArithmeticModel {

    private StringBuilder Addition;

    public String setAddition(String binary1, String binary2) {

        Addition = new StringBuilder();
        int i = binary1.length() - 1;
        int j = binary2.length() - 1;
        int carry = 0;
        while (i >= 0 || j >= 0) {
            int sum = carry;
            if (j >= 0) {
                sum += binary2.charAt(j--) - '0' ;
            }
            if (i >= 0) {
                sum += binary1.charAt(i--) - '0' ;
            }

            // Added number can be only 0 or 1
            Addition.append(sum % 2);

            // Get the carry.
            carry = sum / 2;
        }

        if (carry != 0) {
            Addition.append(carry);
        }

        // First reverse and then return.
        return Addition.reverse().toString();

    }

    public StringBuilder getAddition() {
        return Addition;
    }

    private StringBuilder Substraction;


    public String setSubstraction(String binary1, String binary2) {
        Substraction = new StringBuilder();
        String max;
        char binary='0';

        boolean tf=(binary1.length()>=binary2.length());

        int paramA=binary1.length();
        int paramB=binary2.length();

        if(paramA<paramB) {

            StringBuilder binary1Builder = new StringBuilder(binary1);
            for (int a = 1; a <= paramB - paramA; a++)
                binary1Builder.insert(0, '0');
            binary1 = binary1Builder.toString();

        } else if(paramB<paramA) {

            StringBuilder binary2Builder = new StringBuilder(binary2);
            for (int a = 1; a <= paramA - paramB; a++)
                binary2Builder.insert(0, "0");
            binary2 = binary2Builder.toString();
        }


        if(!tf) {

            for (int a = paramA - 1; a >= 0; a--) {

                if (binary1.charAt(a) != binary2.charAt(a)) {

                    if (binary2.charAt(a) == '1') {

                        max = binary2;
                        binary2 = binary1;
                        binary1 = max;
                        break;

                    }
                }
            }
        }

        for(int a=binary1.length()-1;a>=0;a--){

            if(binary1.charAt(a)=='1' && binary2.charAt(a)=='0'){

                if(binary=='1') {

                    Substraction.insert(0, '0');

                    binary='0';

                }else{

                    Substraction.insert(0, '1');

                }

            }else if(binary1.charAt(a)==binary2.charAt(a) && binary2.charAt(a)=='1'){

                if(binary=='1'){

                    Substraction.insert(0, '1');

                    binary='1';

                }else{
                    Substraction.insert(0, '0');
                }

            }else if(binary1.charAt(a)=='0' && binary2.charAt(a)=='1'){

                if(binary=='1'){

                    Substraction.insert(0, '0');

                }else{

                    Substraction.insert(0, '1');

                    binary = '1';

                }

            } else {

                if(binary=='1'){

                    Substraction.insert(0, '1');

                }else{

                    Substraction.insert(0, '0');

                }
            }
        }

        return Substraction.toString();
    }

    public StringBuilder getSubstraction() {
        return Substraction;
    }

    private int Multiply = 0;

    public int setMultiply(int binary1 , int binary2) {
        int i=0;
        int remainder = 0;
        int[] sum = new int[20];

        while(binary1!=0||binary2!=0){
            sum[i++] =  (binary1 %10 + binary2 %10 + remainder ) % 2;
            remainder = (binary1 %10 + binary2 %10 + remainder ) / 2;
            binary1 = binary1/10;
            binary2 = binary2/10;
        }

        if(remainder!=0)
            sum[i++] = remainder;
        --i;
        while(i>=0)
            Multiply = Multiply*10 + sum[i--];

        return Multiply;
    }

    public int getMultiply() {
        return Multiply;
    }

    private String Divide;

    public String setDivide(int binary1, int binary2) {

        int binaryOne = Integer.parseInt(String.valueOf(binary1),2);

        int binaryTwo = Integer.parseInt(String.valueOf(binary2),2);

        Divide = Integer.toBinaryString((binaryOne/binaryTwo));

        return Divide;

    }

    public String getDivide() {
        return Divide;
    }

}
