package Model;

public class BitwiseModel {

    private int AND;

    public void setAND(int firstbit, int secondbit){
        AND = firstbit & secondbit;
    }

    public int getAND(){
        return AND;
    }

    private int OR;

    public void setOR(int firstbit, int secondbit){
        OR = firstbit | secondbit;
    }

    public int getOR(){
        return OR;
    }

    private int XOR;

    public void setXOR(int firstbit, int secondbit){
        XOR = firstbit ^ secondbit;
    }

    public int getXOR() {
        return XOR;
    }

    private double LeftShift;

    public void setLeftShift(int firstbit, int secondbit){
        LeftShift = firstbit << 2;
    }

    public double getLeftShift() {
        return LeftShift;
    }

    private int RightShift;

    public void setRightShift(int firstbit, int secondbit){
        RightShift = firstbit >> 2;
    }

    public int getRightShift() {
        return RightShift;
    }
}
