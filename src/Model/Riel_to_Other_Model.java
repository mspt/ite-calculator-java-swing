package Model;

public class Riel_to_Other_Model {

    private float USD;

    public void setUSD(float riel){
        USD = riel / 4000;
    }

    public float getUSD(){
        return USD;
    }

    private float Euro;

    public void setEuro(float riel){
        Euro = (float) (riel / 4628.35);
    }

    public float getEuro(){
        return Euro;
    }

    private float Frank;

    public void setFrank(float riel){
        Frank = (float) (riel / 4449.87);
    }

    public float getFrank(){
        return Frank;
    }

    private float Pound;

    public void setPound(float riel){
        Pound = (float) (riel / 5534.54);
    }

    public float getPound(){
        return Pound;
    }

    private float Baht;

    public void setBaht(float riel){
        Baht = (float) (riel / 123.05);
    }

    public float getBaht(){
        return Baht;
    }
}
