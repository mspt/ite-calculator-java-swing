package Model;

/*
  ITE_LAB7
  Made By : Mekmun Sopheaktra
  Date: 10/02/2022
  MoneyExchange class
  @since 7.0
 */
public class MoneyExchange {
    /**
     * This Method use for convert Riel to USD
     * @param Riel use input in riel
     * @return Result
     */
    public static double RieltoUSD(double Riel){
        //Return Riel to USD Result
        return Riel/4000;
    }

    /**
     * This Method use for convert Riel to Euro
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoEuro(double Riel){
        //Return Riel to Euro Result
        return Riel/4628.35;
    }

    /**
     * This Method use for convert Riel to Frank
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoFrank(double Riel){
        //Return Riel to Frank Result
        return Riel/4449.87;
    }

    /**
     * This Method use for convert Riel to Pound
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoPound(double Riel){
        //Return Riel to Pound Result
        return Riel/5534.54;
    }

    /**
     * This Method use for convert Riel to Baht
     * @param Riel user input in riel
     * @return Result
     */
    public static double RieltoBaht(double Riel){
        //Return Riel to Baht Result
        return Riel/123.05;
    }

    /**
     * This Method use for convert USD to Riel
     * @param USD user input in USD
     * @return Result
     */
    public static double USDtoRiel(double USD){
        //Return USD to Riel Result
        return USD*4000;
    }

    /**
     * This Method use for convert Euro to Riel
     * @param Euro user input in Euro
     * @return Result
     */
    public static double EurotoRiel(double Euro){
        //Return Euro to Riel Result
        return Euro*4628.35;
    }

    /**
     * This Method use for convert Frank to Riel
     * @param Frank user input in frank
     * @return Result
     */
    public static double FranktoRiel(double Frank){
        //Return Frank to Riel Result
        return Frank*4449.87;
    }

    /**
     * This Method use for convert Pound to Riel
     * @param Pound user input in pound
     * @return Result
     */
    public static double PoundtoRiel(double Pound){
        //Return Pound to Riel Result
        return Pound*5534.54;
    }

    /**
     * This Method use for convert Baht to Riel
     * @param Baht user input in baht
     * @return Result
     */
    public static double BahttoRiel(double Baht){
        //Return Baht to Riel Result
        return Baht*123.05;
    }
}
