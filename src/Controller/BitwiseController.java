package Controller;

import Model.BitwiseModel;
import Model.CalcModel;
import View.BitwiseView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BitwiseController {

    private BitwiseView bitwiseView;
    private BitwiseModel bitwiseModel;
    private JTextField jTextField;

    public JTextField getjTextField() {
        return jTextField;
    }

    public void setjTextField(JTextField jTextField) {
        this.jTextField = jTextField;
    }

    String myProblem = "must be input 2 integers.";

    public BitwiseController(BitwiseView bitwiseView, BitwiseModel bitwiseModel){

        this.bitwiseView = bitwiseView;
        this.bitwiseModel = bitwiseModel;

        this.bitwiseView.setANDButton(new BitwiseController.ANDCal());
        this.bitwiseView.setORButton(new BitwiseController.ORCal());
        this.bitwiseView.setXORButton(new BitwiseController.XORCal());
        this.bitwiseView.setLeftButton(new BitwiseController.LeftCal());
        this.bitwiseView.setRightButton(new BitwiseController.RightCal());
        this.bitwiseView.setkey(new keynumberonly());
    }

    class ANDCal implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            int firstNumber, secondNumber = 0;

            try{
                firstNumber = bitwiseView.getFirstbit();
                secondNumber = bitwiseView.getSecondbit();

                bitwiseModel.setAND(firstNumber, secondNumber);
                bitwiseView.setANDOUT(bitwiseModel.getAND());
            }
            catch(NumberFormatException ex){
                bitwiseView.displayErrorMessage(myProblem);
            }
        }

    }

    class ORCal implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            int firstNumber, secondNumber = 0;

            try{
                firstNumber = bitwiseView.getFirstbit();
                secondNumber = bitwiseView.getSecondbit();

                bitwiseModel.setOR(firstNumber, secondNumber);
                bitwiseView.setOROUT(bitwiseModel.getOR());
            }
            catch(NumberFormatException ex){
                bitwiseView.displayErrorMessage(myProblem);
            }
        }
    }

    class XORCal implements java.awt.event.ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0){
            int firstNumber, secondNumber = 0;

            try{
                firstNumber = bitwiseView.getFirstbit();
                secondNumber = bitwiseView.getSecondbit();

                bitwiseModel.setXOR(firstNumber, secondNumber);
                bitwiseView.setXOROUT(bitwiseModel.getXOR());
            }
            catch(NumberFormatException ex){
                bitwiseView.displayErrorMessage(myProblem);
            }
        }
    }

    class LeftCal implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0){
            int firstNumber, secondNumber = 0;

            try{
                firstNumber = bitwiseView.getFirstbit();
                secondNumber = bitwiseView.getSecondbit();

                bitwiseModel.setLeftShift(firstNumber, secondNumber);
                bitwiseView.setLeftShiftOUT((int) bitwiseModel.getLeftShift());
            }
            catch(NumberFormatException ex){
                bitwiseView.displayErrorMessage(myProblem);
            }
        }
    }

    class RightCal implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0){
            int firstNumber, secondNumber = 0;

            try{
                firstNumber = bitwiseView.getFirstbit();
                secondNumber = bitwiseView.getSecondbit();

                bitwiseModel.setRightShift(firstNumber, secondNumber);
                bitwiseView.setRightShiftOUT(bitwiseModel.getRightShift());
            }
            catch(NumberFormatException ex){
                bitwiseView.displayErrorMessage(myProblem);
            }
        }
    }

    public static class keynumberonly{
        public void key(JTextField jTextField) {
            jTextField.addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent e) {
                    char c = e.getKeyChar();
                    if (((c < '0') || (c > '1')) && (c != KeyEvent.VK_BACK_SPACE)) {
                        e.consume();  // if it's not a number, ignore the event
                    }
                }
            });
        }
    }
}
