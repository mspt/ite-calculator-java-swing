package Controller;

import Model.Other_to_Riel_Model;
import Model.Riel_to_Other_Model;
import View.Other_to_Riel;
import View.Riel_to_other;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Other_to_Riel_Controller {

    private Other_to_Riel other_to_riel;
    private Other_to_Riel_Model other_to_riel_model;
    private JTextField jTextField;

    public JTextField getjTextField() {
        return jTextField;
    }

    public void setjTextField(JTextField jTextField) {
        this.jTextField = jTextField;
    }

    String myProblem = "must be input a integer.";

    public Other_to_Riel_Controller(Other_to_Riel other_to_riel, Other_to_Riel_Model other_to_riel_model){

        this.other_to_riel = other_to_riel;
        this.other_to_riel_model = other_to_riel_model;


        this.other_to_riel.setUSDButton(new Other_to_Riel_Controller.USD());
        this.other_to_riel.setEuroButton(new Other_to_Riel_Controller.Euro());
        this.other_to_riel.setFrankButton(new Other_to_Riel_Controller.Frank());
        this.other_to_riel.setPoundButton(new Other_to_Riel_Controller.Pound());
        this.other_to_riel.setBahtButton(new Other_to_Riel_Controller.Baht());

        this.other_to_riel.setkey(new Other_to_Riel_Controller.keynumberonly());
    }

    class USD implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float usd;

            try {
                usd = other_to_riel.getUSD();

                other_to_riel_model.setUSD(usd);
                other_to_riel.setUSDtoRiel(other_to_riel_model.getUSD());
            }
            catch(NumberFormatException ex){
                other_to_riel.displayErrorMessage(myProblem);
            }
        }

    }

    class Euro implements java.awt.event.ActionListener {


        @Override
        public void actionPerformed(ActionEvent arg0){
            float euro;

            try {
                euro = other_to_riel.getUSD();

                other_to_riel_model.setEuro(euro);
                other_to_riel.setEUROtoRiel(other_to_riel_model.getEuro());
            }
            catch(NumberFormatException ex){
                other_to_riel.displayErrorMessage(myProblem);
            }
        }


    }

    class Frank implements java.awt.event.ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0){
            float frank;

            try {
                frank = other_to_riel.getUSD();

                other_to_riel_model.setFrank(frank);
                other_to_riel.setFranktoRiel(other_to_riel_model.getFrank());
            }
            catch(NumberFormatException ex){
                other_to_riel.displayErrorMessage(myProblem);
            }
        }

    }

    class Pound implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float pound;

            try {
                pound = other_to_riel.getPound();

                other_to_riel_model.setPound(pound);
                other_to_riel.setPoundtoRiel(other_to_riel_model.getFrank());
            }
            catch(NumberFormatException ex){
                other_to_riel.displayErrorMessage(myProblem);
            }
        }

    }

    class Baht implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float baht;

            try {
                baht = other_to_riel.getBaht();

                other_to_riel_model.setBaht(baht);
                other_to_riel.setBahttoRiel(other_to_riel_model.getBaht());
            }
            catch(NumberFormatException ex){
                other_to_riel.displayErrorMessage(myProblem);
            }
        }

    }


    public static class keynumberonly{
        public void key(JTextField jTextField) {
            jTextField.addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent e) {
                    char c = e.getKeyChar();
                    if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                        e.consume();  // if it's not a number, ignore the event
                    }
                }
            });
        }
    }
}
