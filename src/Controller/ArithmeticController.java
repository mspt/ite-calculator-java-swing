package Controller;


import Model.ArithmeticModel;
import View.Arithmetic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;


public class ArithmeticController extends AbstractAction {


    Arithmetic view;
    ArithmeticModel calc;
    public ArithmeticController(Arithmetic view,ArithmeticModel calc){
        this.view = view;
        this.calc = calc;
    }

    //ArithmeticModel calc = new ArithmeticModel();

    JTextField display1;
    JTextField display2;

    JButton dotButton;



    //String str = "";
    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        Arithmetic viewController = new Arithmetic();
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "1","2","3","4","5","6","7","8","9","0" -> {
                view.getDisplay2().setText(view.getDisplay2().getText().concat(actionCommand));
            }
        }
        System.out.println(actionCommand);


        int number = -1;

        try {  // checks if number is > -1 and < 10
            number = Integer.parseInt (actionCommand);

        } catch (NumberFormatException e1) {
        }


        if (number >= 0 && calc.getErrorState()== false ) {
            view.getDisplay2().setText("");
            view.str += actionCommand;
            view.getDisplay2().setText(view.str);
        }



        if ("+".equals(actionCommand) && !calc.getErrorState()){ // check if addition
            view.getDisplay1().setText(actionCommand);
            calc.setArithmeticOp("+");

            if(!view.str.isEmpty()){ // checks if string is not empty
                calc.setOperand1(view.str); // sets it to both operands
                calc.setOperand2(view.str);
            }
            view.getDisplay1().setText(calc.getOperand1()+ " +");
            view.str = "";

        } else if ("-".equals(actionCommand)&& !calc.getErrorState()){ // checks for subtraction
            view.getDisplay1().setText(actionCommand);
            calc.setArithmeticOp("-");
            if(!view.str.isEmpty()){
                calc.setOperand1(view.str);
                calc.setOperand2(view.str);
            }
            view.getDisplay1().setText(calc.getOperand1()+ " -");
            view.str = "";
        } else if ("*".equals(actionCommand)&& !calc.getErrorState()){ // checks for multiplication
            view.getDisplay1().setText(actionCommand);
            calc.setArithmeticOp("*");
            if(!view.str.isEmpty()){
                calc.setOperand1(view.str);
                calc.setOperand2(view.str);
            }
            view.getDisplay1().setText(calc.getOperand1()+ " *");

            view.str = "";

        } else if ("/".equals(actionCommand) && !calc.getErrorState()){ // checks for division
            view.getDisplay1().setText(actionCommand);
            calc.setArithmeticOp("/");
            if(!view.str.isEmpty()){
                calc.setOperand1(view.str);
                calc.setOperand2(view.str);
            }
            view.getDisplay1().setText(calc.getOperand1()+ " /");
            view.str = "";

        } else if ("=".equals(actionCommand)&& !calc.getErrorState()){ // get the total for arithmetic operation
            System.out.print(calc.getTotal());
            view.getDisplay2().setText(calc.getTotal());
            if ( calc.getTotal().equals("")){ // if only = button is clicked , do nothing
                view.str = "";

                return;
            }

            if (calc.getTotal().equals("/") && view.str.equals("0")){ // if any number is divided by zero , generate error
                if (calc.getOperand1().equals("0")){
                    view.getDisplay2().setText("Result is undefined");
                } else {
                    view.getDisplay2().setText("cannot divide by zero");
                }
                calc.setErrorState(true);

            } else {
                if (!view.str.isEmpty()){
                    calc.setOperand2(view.str);

                }
                view.getDisplay1().setText("");
                view.getDisplay2().setText((calc.precesion()));
                calc.setOperand1(calc.precesion());
                System.out.println(calc.getTotal());
                view.str ="";
            }

        } else if ("C".equals(actionCommand)){

            if (calc.getMode().equals("Int")){
                calc.reset();
                view.getDisplay1().setText("");
                view.getDisplay2().setText("0");
                view.str = "";
            } else {
                calc.reset();
                view.getDisplay1().setText("");
                view.getDisplay2().setText("0");
                view.str = "";

                calc.setErrorState(false);
            }
        } else if ("\u21B2".equals(actionCommand)&& !calc.getErrorState()){
            StringBuilder strB = new StringBuilder(view.str);

            if (view.str.length() > 0) {

                if(strB.toString().length()== 1 || strB.toString().length()== 2 && view.getDisplay2().getText().contains("-")){
                    view.getDisplay2().setText("0");
                    view.str = "";
                }
                else {
                    strB.deleteCharAt(view.str.length() - 1);
                    view.str = strB.toString();
                    view.getDisplay2().setText(view.str);
                }
            }

        } else if (".".equals(actionCommand)&& !calc.getErrorState()){
            if (view.str.contains(".")){
                return;
            }

            view.str += actionCommand;
            view.getDisplay2().setText(view.str);

        } else if ("\u00B1".equals(actionCommand)&& !calc.getErrorState()){
            if (view.str.length()==0){
                view.getDisplay2().setText("0");
                view.getDisplay1().setText("negate(0)");
            }

            if (view.str.length()> 0){
                StringBuilder strB = new StringBuilder(view.str);
                if (view.str.contains("-")){
                    strB.deleteCharAt(0);
                    view.str = strB.toString();
                    view.getDisplay2().setText(view.str);
                } else {
                    view.str = "-" + view.str;
                    view.getDisplay2().setText(view.str);
                }
            }
        }
    }

}