package Controller;

import Model.Riel_to_Other_Model;
import View.Riel_to_other;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Riel_to_Other_Controller {

    private Riel_to_other riel_to_other;
    private Riel_to_Other_Model riel_to_other_model;
    private JTextField jTextField;

    public JTextField getjTextField() {
        return jTextField;
    }

    public void setjTextField(JTextField jTextField) {
        this.jTextField = jTextField;
    }

    String myProblem = "must be input a integer.";

    public Riel_to_Other_Controller(Riel_to_other riel_to_other, Riel_to_Other_Model riel_to_other_model){

        this.riel_to_other = riel_to_other;
        this.riel_to_other_model = riel_to_other_model;


        this.riel_to_other.setSubmit(new Riel_to_Other_Controller.USD());
        this.riel_to_other.setSubmit(new Riel_to_Other_Controller.Euro());
        this.riel_to_other.setSubmit(new Riel_to_Other_Controller.Frank());
        this.riel_to_other.setSubmit(new Riel_to_Other_Controller.Pound());
        this.riel_to_other.setSubmit(new Riel_to_Other_Controller.Baht());

        this.riel_to_other.setkey(new keynumberonly());
    }

    class USD implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float riel;

            try {
                riel = riel_to_other.getRiel();

                riel_to_other_model.setUSD(riel);
                riel_to_other.setUSD(riel_to_other_model.getUSD());
            }
           catch(NumberFormatException ex){
                riel_to_other.displayErrorMessage(myProblem);
            }
        }

    }

    class Euro implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float riel;

            try {
                riel = riel_to_other.getRiel();

                riel_to_other_model.setEuro(riel);
                riel_to_other.setEuro(riel_to_other_model.getEuro());
            }
            catch(NumberFormatException ex){
                riel_to_other.displayErrorMessage(myProblem);
            }
        }

    }

    class Frank implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float riel;

            try {
                riel = riel_to_other.getRiel();

                riel_to_other_model.setFrank(riel);
                riel_to_other.setFrank(riel_to_other_model.getFrank());
            }
            catch(NumberFormatException ex){
                riel_to_other.displayErrorMessage(myProblem);
            }
        }

    }

    class Pound implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float riel;

            try {
                riel = riel_to_other.getRiel();

                riel_to_other_model.setPound(riel);
                riel_to_other.setPound(riel_to_other_model.getPound());
            }
            catch(NumberFormatException ex){
                riel_to_other.displayErrorMessage(myProblem);
            }
        }

    }

    class Baht implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            float riel;

            try {
                riel = riel_to_other.getRiel();

                riel_to_other_model.setBaht(riel);
                riel_to_other.setBaht(riel_to_other_model.getBaht());
            }
            catch(NumberFormatException ex){
                riel_to_other.displayErrorMessage(myProblem);
            }
        }

    }


    public static class keynumberonly{
        public void key(JTextField jTextField) {
            jTextField.addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent e) {
                    char c = e.getKeyChar();
                    if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
                        e.consume();  // if it's not a number, ignore the event
                    }
                }
            });
        }
    }
}
