package Controller;

import Model.BinaryArithmeticModel;
import View.BinaryarithmeticView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class BinaryArithmeticController {

    private BinaryarithmeticView binaryarithmeticView;

    private BinaryArithmeticModel binaryArithmeticModel;
    private JTextField jTextField;

    public JTextField getjTextField() {
        return jTextField;
    }

    public void setjTextField(JTextField jTextField) {
        this.jTextField = jTextField;
    }

    String myProblem = "must be input a integer.";

    public BinaryArithmeticController(BinaryarithmeticView binaryarithmeticView, BinaryArithmeticModel binaryArithmeticModel){

        this.binaryarithmeticView = binaryarithmeticView;
        this.binaryArithmeticModel = binaryArithmeticModel;


        this.binaryarithmeticView.setSubmit(new BinaryArithmeticController.addition());
        this.binaryarithmeticView.setSubmit(new BinaryArithmeticController.subtraction());
        this.binaryarithmeticView.setSubmit(new BinaryArithmeticController.multiply());
        this.binaryarithmeticView.setSubmit(new BinaryArithmeticController.divide());

        this.binaryarithmeticView.setkey(new BinaryArithmeticController.keynumberonly());
    }

    class addition implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            String binary1;
            String binary2;


            try {
                binary1 = String.valueOf(binaryarithmeticView.getFirstbit());
                binary2 = String.valueOf(binaryarithmeticView.getSecondbit());

                binaryArithmeticModel.setAddition(binary1,binary2);
                binaryarithmeticView.setAddition(String.valueOf(binaryArithmeticModel.getAddition()));
            }
            catch(NumberFormatException ex){
                binaryarithmeticView.displayErrorMessage(myProblem);
            }
        }

    }

    class subtraction implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            String binary1;
            String binary2;


            try {
                binary1 = String.valueOf(binaryarithmeticView.getFirstbit());
                binary2 = String.valueOf(binaryarithmeticView.getSecondbit());

                binaryArithmeticModel.setSubstraction(binary1,binary2);
                binaryarithmeticView.setSubtraction(String.valueOf(binaryArithmeticModel.getSubstraction()));
            }
            catch(NumberFormatException ex){
                binaryarithmeticView.displayErrorMessage(myProblem);
            }
        }

    }

    class multiply implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            int binary1;
            int binary2;


            try {
                binary1 = binaryarithmeticView.getFirstbit();
                binary2 = binaryarithmeticView.getSecondbit();

                binaryArithmeticModel.setMultiply(binary1,binary2);
                binaryarithmeticView.setMultiplyBinary(binaryArithmeticModel.getMultiply());
            }
            catch(NumberFormatException ex){
                binaryarithmeticView.displayErrorMessage(myProblem);
            }
        }

    }

    class divide implements java.awt.event.ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0){
            int binary1;
            int binary2;


            try {
                binary1 = binaryarithmeticView.getFirstbit();
                binary2 = binaryarithmeticView.getSecondbit();

                binaryArithmeticModel.setDivide(binary1,binary2);
                binaryarithmeticView.setDivideBinary(Integer.parseInt(binaryArithmeticModel.getDivide()));
            }
            catch(NumberFormatException ex){
                binaryarithmeticView.displayErrorMessage(myProblem);
            }
        }
    }


    public static class keynumberonly{
        public void key(JTextField jTextField) {
            jTextField.addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent e) {
                    char c = e.getKeyChar();
                    if (((c < '0') || (c > '1')) && (c != KeyEvent.VK_BACK_SPACE)) {
                        e.consume();  // if it's not a number, ignore the event
                    }
                }
            });
        }
    }
}
